{ config, pkgs, ... }:

{
  imports =
    [ 
      ./hardware-configuration.nix
    ];

  nix = {
    package = pkgs.nixUnstable; 
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  nixpkgs = {
    config = {
      allowUnfree = true;
      allowBroken = false;
    };
  }; 

  programs = {
    gnupg = {
      agent = {
        enable = true;
        enableSSHSupport = true;
      };
    };
  };

  boot = {
    loader = {
      systemd-boot = { enable = true; };
      efi = { canTouchEfiVariables = true; };
      grub = { useOSProber = true; };
    };
  };

  networking = {
    hostName = "gilberto";
    networkmanager = { enable = true; };
  };

  time = {
    timeZone = "America/Argentina/Buenos_Aires";
    hardwareClockInLocalTime = true;
  };

  i18n = { defaultLocale = "en_US.UTF-8"; };

  services = {
    openssh = { enable = false; };
    dnsmasq = { enable = false; };
    picom = { enable = true; };
    gnome = { gnome-keyring = { enable = true; }; };
    xserver = {
      enable = true;
      layout = "es";
      displayManager = {
        lightdm = { 
          enable = true; 
          # mini.enable = true;  
        };
        defaultSession = "none+awesome";
        sessionCommands = '' 
          xset s off
          xset -dpms
          xset s noblank
          xset r rate 210 40
        '';
      };
      windowManager = {
        awesome = {
          enable = true;
          luaModules = with pkgs.luaPackages; [
            luarocks 
          ];
        };
      };
    };
  };

  virtualisation = {
    virtualbox = {
      host = {
        enable = true;
        enableExtensionPack = true;
      };
    };
    libvirtd = {
      enable = true;
      onShutdown = "shutdown";
      onBoot = "start";
      allowedBridges = [ "virbr0" ];
      qemu = {
        package = pkgs.qemu_kvm;
      };
    };
    docker = {
      enable = true;
      enableOnBoot = false;
    };
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "es";
  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users = {
    users = {
      gilberto = {
        isNormalUser = true;
        extraGroups = [ "wheel" "networkmanager" "vboxusers" "libvirtd" "qemu-libvirtd" ]; 
        initialPassword = "gilberto";
        shell = pkgs.zsh;
      };
    }; 
  };

  fonts = {
    fontDir = { enable = true; };
    fonts = with pkgs; [
      ttf_bitstream_vera
      cantarell-fonts
      liberation_ttf
      cascadia-code
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      fira-code
      fira-code-symbols
      fira-mono
      dina-font
      proggyfonts 
      inconsolata
      anonymousPro
      hack-font
      roboto
      (nerdfonts.override { 
        fonts = [ "FiraCode" "UbuntuMono" "JetBrainsMono" "Meslo" "RobotoMono" ]; 
      })
    ];
  };

  environment = {
    sessionVariables = {
      XDG_CACHE_HOME  = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME   = "$HOME/.local/share";
      XDG_BIN_HOME    = "$HOME/.local/bin";
    };
    
    etc."xdg/user-dirs.defaults".text = ''
      DESKTOP=Desktop
      DOWNLOAD=Downloads
      TEMPLATES=Templates
      PUBLICSHARE=Public
      DOCUMENTS=Documents
      MUSIC=Music
      PICTURES=Pictures
      VIDEOS=Videos
    '';

    variables = { 
      PATH = "$HOME/.bin:$PATH"; 
      EDITOR = "vim";
    };

    systemPackages = with pkgs; [
      vim 
      wget
      brave
      zip
      unzip
      git
      vscode
      nitrogen
      kitty
      gnumake
      pcmanfm
      xdg-user-dirs
      gnome3.evince
      gnome3.eog
      lxappearance
      qalculate-gtk
    ];
  };

  system.stateVersion = "unstable"; 
}

