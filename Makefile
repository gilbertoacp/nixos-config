build:
	nixos-rebuild build --flake ".#"

switch:
	sudo nixos-rebuild switch --flake ".#"  

switch-gilbertoacp:
	nix build .#homeManagerConfigurations.gilberto.activationPackage
	./result/activate
